package org.ratina.util;

/**
 * Created by kj on 17-2-25.
 */

/**
 * Pure tuple implementation
 * @param <Fst>
 * @param <Snd>
 */

public class Tuple<Fst, Snd> {
    final Fst _fst;
    final Snd _snd;

    public Tuple(Fst fst, Snd snd) {
        _fst = fst;
        _snd = snd;
    }

    public Fst fst() {
        return _fst;
    }

    public Snd snd() {
        return _snd;
    }
}
