package org.ratina.mvpdemo;

import org.ratina.util.Tuple;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kj on 17-2-25.
 */

interface ItemGetter<PresenterType, T> {
    T getItem(PresenterType p);
}

interface ItemPublisher<ViewType, T> {
    void publishItem(ViewType v, T item);
}

public abstract class AbstractPresenter<ViewType> {

    ViewType _view;
    final List<Tuple<ItemGetter, ItemPublisher>> _publishStrategies;

    public AbstractPresenter() {
        _publishStrategies = publishStrategies();
    }

    public synchronized void attachView(ViewType view) {
        _view = view;
    }

    public synchronized void detachView() {
        _view = null;
    }

    public synchronized void publish() {
        if (_view != null) {
            for (Tuple<ItemGetter, ItemPublisher> strategy : _publishStrategies) {
                ItemGetter itemGetter = strategy.fst();
                ItemPublisher itemPublisher = strategy.snd();
                Object item = itemGetter.getItem(this);
                if (item != null) {
                    itemPublisher.publishItem(_view, item);
                }
            }
        }
    }

    public abstract List<Tuple<ItemGetter, ItemPublisher>> publishStrategies();
}
