package org.ratina.mvpdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    MainPresenter _p;
    ArrayAdapter<String> _someListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _p = new MainPresenter();
        _someListAdapter = new ArrayAdapter<>(this, R.layout.somelist_text);
        ListView lv = (ListView) findViewById(R.id.some_list_list_view);
        lv.setAdapter(_someListAdapter);
        _p.attachView(this);
        _p.publish();
    }

    public void setSomeText(String s) {
        TextView tv = (TextView) findViewById(R.id.some_text_text_view);
        tv.setText(s);
    }

    public void setSomeList(ArrayList<String> l) {
        _someListAdapter.clear();
        _someListAdapter.addAll(l);
    }
}
