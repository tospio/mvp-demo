package org.ratina.mvpdemo;

import org.ratina.util.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kj on 17-2-25.
 */

public class MainPresenter extends AbstractPresenter<MainActivity> {

    String _someText;
    ArrayList<String> _someList = new ArrayList<>();

    public MainPresenter() {
        super();
        Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Long value) {
                        _someList.add(value.toString());
                        publish();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public static String getSomeText(MainPresenter p) {
        return p._someText;
    }

    public static ArrayList<String> getSomeList(MainPresenter p) {
        return p._someList;
    }

    @Override
    public List<Tuple<ItemGetter, ItemPublisher>> publishStrategies() {
        Tuple<ItemGetter<MainPresenter, String>, ItemPublisher<MainActivity, String>> someTextStrategy
                = new Tuple<ItemGetter<MainPresenter, String>, ItemPublisher<MainActivity, String>>(
                new ItemGetter<MainPresenter, String>() {
            @Override
            public String getItem(MainPresenter p) {
                return MainPresenter.getSomeText(p);
            }
        }, new ItemPublisher<MainActivity, String>() {
            @Override
            public void publishItem(MainActivity v, String item) {
                v.setSomeText(item);
            }
        });

        Tuple<ItemGetter<MainPresenter, ArrayList<String>>, ItemPublisher<MainActivity, ArrayList<String>>> someListStrategy
                = new Tuple<ItemGetter<MainPresenter, ArrayList<String>>, ItemPublisher<MainActivity, ArrayList<String>>>(
                new ItemGetter<MainPresenter, ArrayList<String>>() {
                    @Override
                    public ArrayList<String> getItem(MainPresenter p) {
                        return MainPresenter.getSomeList(p);
                    }
                },
                new ItemPublisher<MainActivity, ArrayList<String>>() {
                    @Override
                    public void publishItem(MainActivity v, ArrayList<String> item) {
                        v.setSomeList(item);
                    }
                }
        );

        ArrayList strategyList = new ArrayList<>();
        strategyList.add(someTextStrategy);
        strategyList.add(someListStrategy);
        return strategyList;
    }
}
